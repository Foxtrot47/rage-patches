//
// audioengine/enginedefs.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef AUD_ENGINE_DEFS_H
#define AUD_ENGINE_DEFS_H

#define AUD_SUPPORT_RAVE_EDITING (__USEDEBUGAUDIO)

#if AUD_SUPPORT_RAVE_EDITING
#define SUPPORT_RAVE_EDITING_ONLY(x) x
#else
#define SUPPORT_RAVE_EDITING_ONLY(x)
#endif

#define AUD_DEBUG_SOUNDS (AUD_SUPPORT_RAVE_EDITING && !__SPU) // use the variance cache to store the extra debug info

namespace rage {


// PURPOSE
//	Defines the maximum number of sound manager (global) variables
static const u32 g_MaxSoundManagerVariables = 64;

static const u32 g_MaxAudEntityVariableBlocks = 256;

// PURPOSE
//	Defines the maximum number of effects that can be active at any one time
static const u32 g_MaxManagedEffects = 24;

// 0xff denotes null so limit to 255 slots per bucket
const u32 g_audMaxSoundSlotsPerBucket = 255;
const u32 g_audMaxRequestedSettingsSlotsPerBucket = 96;
// this is the maximum number of dynamic children that can be instantiated per frame per bucket
const u32 g_audMaxChildSoundRequestsPerBucket = 64;
// this is the maximum number of voices that can be used by any one sound bucket
const u32 g_audMaxVoicesPerBucket = 96;

// The number of shadow sound pairings per bucket
const u32 g_audMaxShadowSoundPairs = 64;

#if RSG_PC || RSG_ORBIS || RSG_DURANGO
#define NUM_REQUEST_BUFFERS 4
const u32 g_audRequestedSettingsBuffers = NUM_REQUEST_BUFFERS;
#else
const u32 g_audRequestedSettingsBuffers = 3;
#endif


const u32 g_NumAudioTimers = 7;



// PURPOSE
//  Variables are stored as name-value pairs
struct variableNameValuePair
{
	u32 hash;
	f32 value;
};



struct audTimerState
{
	audTimerState() : timeInMs(0), timeScale(1.f), isPaused(false), wasPausedLastFrame(false){}
	
	u32 timeInMs;
	f32 timeScale;
	bool isPaused;
	bool wasPausedLastFrame;
};

struct audEngineContext
{
	u32 systemTimeMs;
	audTimerState timers[g_NumAudioTimers];
};


}	// namespace rage

#endif // AUD_ENGINE_DEFS_H
