### Glourious Turnpike Adventure V (Project Rush) patchset

Drop these into X:\crazyracer2\src\ and you should be good
Remember to disable Werror.

--- Optional notes:
launch params: -noSocialClub -nokeyboardhook -nonetlogs
-noSocialClub - fixes activation issues
-nokeyboardhook - stops the game from eating keystrokes when frozen
-nonetlogs - fixes a crash with message.log

optional launch params:
-output - adds debug console output
-kbgame - defaults to gameplay keyboard instead of debug keys

old launch params that aren't required anymore:
-useFinalShaders - NOT NEEDED, debug shaders are already provided and final shaders do not work with the debug build
-audiopack - NOT NEEDED, the game already defaults to audio_rel.rpf with the code changes
-langfilesuffix - NOT NEEDED, the game already defaults to american_rel with the code changes
-commonpack, -nodebugpack, -usepackfiles, -platformpack, -assetsdir - NOT NEEDED, the code changes already make the game default to loading packed files
-nonetwork - NOT RECOMMENDED as it crashes the game whenever you exit the pause menu
-disablecacheloader - NOT NEEDED, the code changes already make the game load the proper cache files

notes:
obviously requires v2699 update and update2 rpfs, compiled debug shaders, and a compiled script.rpf
shaders are loaded from the game folder as loose files in common/shaders, repacking common.rpf is not required
shader quality has to be higher than normal otherwise it loads from win32_40_lq which we don't have compiled yet
press LCtrl+Tab to switch keyboard mode from debug to gameplay, otherwise a controller is required
if you're using dlls from epic games redistributable, place goldberg's socialclub.dll at "C:\Program Files\Rockstar Games\Social Club Debug"